import React, { Fragment, useState } from 'react';
import axios from 'axios';


var decodeHtmlEntity = function(x) {
    return x.replace(/&#(\d+);/g, function(match, dec) {
        return String.fromCharCode(dec);
    });
};
  

const RenderTable = ({ csvData }) => {

    return (
        <table className="table">
        <thead>
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Correo Electronico</th>
                <th scope="col">Telefono</th>
            </tr>
        </thead>
        <tbody>
            {

                csvData.map(row => {
                    row[0] = decodeHtmlEntity(row[0]);
                    row[1] = decodeHtmlEntity(row[1]);
                    return (
                        <tr className={row[3] ? "table-danger" : ""}>
                            <td>{row[0]}</td>
                            <td>{row[1]}</td>
                            <td>{row[2]}</td>
                        </tr>
                    )
                })

            }
        </tbody>
        </table>
    )
}
  


const FileUpload = () => {
    const [file, setFile] = useState();
    const [filename, setFilename] = useState('Documento CSV...');
    const [uploadedFile, setUploadedFile] = useState({});

    const onChange = e => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    }

    const onSubmit = async e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);

        try {
            const res = await axios.post('http://mix.lpm.mx/api/kirana/list', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                use: 'no-cors'
            });

            const { fileName, csvData } = res.data;

            setUploadedFile({ fileName, csvData });
        } catch(err) {
            if(err.response.status === 500) {
                console.log('Problema con el servidor');
            }else{
                console.log(err.response.msg);
            }
        }
    }

    return (
        <Fragment>
            <form onSubmit={onSubmit}>
                <div className="custom-file">
                    <input type="file" className="custom-file-input" id="customFile" onChange={onChange} />
                    <label className="custom-file-label" htmlFor="customFile">{filename}</label>
                </div>                
                <input type="submit" value="Subir" className="btn btn-primary btn-block mt-4" />
            </form>
            { typeof uploadedFile.csvData !== 'undefined' ? 
            <div className="mt-5">
                <h3 className="text-center">{ uploadedFile.fileName }</h3>
                <div><RenderTable csvData={uploadedFile.csvData} /></div>
            </div> : null }
        </Fragment>
    )
}

export default FileUpload;
